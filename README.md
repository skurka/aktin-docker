# AKTIN installer dockerized
```sh
# Build
docker-compose build

# Preinit database
docker-compose up database > log 2>&1 &
DOCKERUP="$!"
until fgrep --quiet "PostgreSQL init process complete; ready for start up." log; do sleep 1; done
kill "$DOCKERUP"
rm log

# Run
docker-compose up
```
